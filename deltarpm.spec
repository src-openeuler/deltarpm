%bcond_without  python3
Name:           deltarpm
Version:        3.6.3
Release:        2
Summary:        deltarpm contains the difference between an old and a new version of an RPM package.
License:        BSD
URL:            https://github.com/rpm-software-management/deltarpm
Source0:        https://github.com/rpm-software-management/deltarpm/archive/3.6.3.tar.gz 

BuildRequires:  gcc bzip2-devel perl-generators xz-devel rpm-devel popt-devel zlib-devel
BuildRequires:  %{_vendor}-rpm-config
BuildRequires:  python3-devel

Patch6000:      backport-fix-CVE-2005-1849.patch
Patch6001:      backport-fix-CVE-2016-9840.patch
Patch6002:      backport-fix-CVE-2016-9841.patch
Patch6003:      backport-fix-CVE-2016-9843.patch
Patch6004:      backport-fix-CVE-2018-25032.patch
Patch6005:      backport-fix-CVE-2022-37434-1.patch
Patch6006:      backport-fix-CVE-2022-37434-2.patch

%description
Delta RPM packages contain the difference between an old and a new
version of an RPM package. Applying a delta RPM on an old RPM results
in the complete new RPM. It is not necessary to have a copy of the
old RPM, because a delta RPM can also work with an installed RPM.

%package help
Summary:       help document
Requires:      %{name} = %{version}-%{release}
BuildArch:     noarch
%description help
This package provides help document for deltarpm.

%package -n python3-%{name}
Summary:        Python3 bindings for deltarpm
BuildRequires:  python3-devel
Requires:       %{name} = %{version}-%{release}
%{?python_provide:%python_provide python3-%{name}}
%description -n python3-%{name}
Python3 bindings for deltarpm.

%package -n drpmsync
Summary:         Sync a file tree with deltarpms
Requires:        %{name}%{_isa} = %{version}-%{release}

%description -n drpmsync
This package contains a tool to sync a file tree with
deltarpms.

%prep
%setup -q -n %{name}-%{version}
#patch for zlib
cd zlib-1.2.2.f-rsyncable
%patch6000 -p1
%patch6001 -p1
%patch6002 -p1
%patch6003 -p1
%patch6004 -p1
%patch6005 -p1
%patch6006 -p1
cd -
#patch for zlib end

%build
%make_build  CFLAGS="$RPM_OPT_FLAGS" LDFLAGS="%__global_ldflags" \
             bindir=%{_bindir} libdir=%{_libdir} mandir=%{_mandir} prefix=%{_prefix} \
             zlibbundled='' zlibldflags='-lz' zlibcppflags=''
%make_build  CFLAGS="$RPM_OPT_FLAGS" LDFLAGS="%__global_ldflags" \
             bindir=%{_bindir} libdir=%{_libdir} mandir=%{_mandir} prefix=%{_prefix} \
             zlibbundled='' zlibldflags='-lz' zlibcppflags='' python

%install
%makeinstall pylibprefix=%{buildroot}

%files
%defattr(-,root,root)
%license LICENSE.BSD
%{_bindir}/*
%exclude %{_bindir}/drpmsync

%files help
%defattr(-,root,root)
%doc README NEWS
%{_mandir}/man8/*

%files -n python3-%{name}
%defattr(-,root,root)
%{python3_sitearch}/%{name}.py
%{python3_sitearch}/__pycache__/%{name}*.pyc
%{python3_sitearch}/_%{name}module.so

%files -n drpmsync
%{_bindir}/drpmsync

%changelog
* Tue Sep 20 2022 zhoushuiqing <zhoushuiqing2@huawei.com> - 3.6.3-2
- Type:CVE
- CVE:CVE-2005-1849,CVE-2016-9840,CVE-2016-9841,CVE-2016-9843,CVE-2018-25032,CVE-2022-37434
- SUG:NA
- DESC:backport patchs fix zlib cves: CVE-2005-1849 CVE-2016-9840
       CVE-2016-9841 CVE-2016-9843 CVE-2018-25032 CVE-2022-37434
       backport-fix-CVE-2005-1849.patch
       backport-fix-CVE-2016-9840.patch
       backport-fix-CVE-2016-9841.patch
       backport-fix-CVE-2016-9843.patch
       backport-fix-CVE-2018-25032.patch
       backport-fix-CVE-2022-37434-1.patch
       backport-fix-CVE-2022-37434-2.patch

* Tue Nov 30 2021 wangjie <wangjie375@huawei.com> - 3.6.3-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Update deltarpm to 3.6.3-1

* Thu Jul 22 2021 wangchen2020 <wangchen137@huawei.com> - 3.6.2-6
- Type:bugfix
- ID: NA
- SUG: NA
- DESC:Delete unnecessary gdb from BuildRequires

* Wed Dec 9 2020 wangchen2020 <wangchen137@huawei.com> - 3.6.2-5
- Type:bugfix
- ID: NA
- SUG: NA
- DESC:correct the invalid URL

* Thu Oct 29 2020 Liquor <lirui130@huawei.com> - 3.6.2-4
- Type:requirement
- ID: NA
- SUG: NA
- DESC:remove python2

* Fri Oct 16 2020 Liquor <lirui130@huawei.com> - 3.6.2-3
- Type:enhancement
- ID: NA
- SUG: NA
- DESC:add drpmsync packages

* Sat Mar 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.6.2-2
- Type:enhancement
- ID: NA
- SUG: NA
- DESC:add build requires of gdb

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.6.2-1
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: update to 3.6.2

* Mon Dec 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.6-30
- Modify email address

* Wed Sep 04 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.6-29
- Package Init
